//
//  LampSerializer.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 18/08/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation

class LampSerializer {
    private static let dateFormatter: DateFormatter = getDateFormatter()
    
    static func toLamp(json: JSON) -> Lamp {
        let id = json["_id"].stringValue
        let label = json["label"].stringValue
        let location = CLLocation(coordinate: CLLocationCoordinate2D(latitude: Double(json["location"]["coordinates"][1].stringValue)!, longitude: Double(json["location"]["coordinates"][0].stringValue)!), altitude: Double(json["location"]["coordinates"][2].stringValue)!)
        
        
        let createdAt = dateFormatter.date(from: json["createdAt"].stringValue)!
        let updatedAt = dateFormatter.date(from: json["updatedAt"].stringValue)!
        
        let newLamp = Lamp(id: id, label: label, location: location, createdAt: createdAt, updatedAt: updatedAt)
        newLamp.street = json["street"].stringValue
        newLamp.height = json["height"].double
        newLamp.overhang = json["overhang"].double
        newLamp.setback = json["setback"].double
        newLamp.armLength = json["arm_length"].double
        newLamp.inclination = json["inclination"].double
        newLamp.lclass = json["lclass"].stringValue
        newLamp.model = json["model"].stringValue
        newLamp.type = json["type"].stringValue
        newLamp.lfr = json["lfr"].double
        newLamp.power = json["power"].double
        newLamp.comment = json["comment"].stringValue
        
        return newLamp
    }
    
    static func toParams(lamp: Lamp) -> Parameters {
        let parameters: Parameters = [
            "label": lamp.label,
            "street": lamp.street ?? "",
            "height": lamp.height as Any,
            "overhang": lamp.overhang as Any,
            "setback": lamp.setback as Any,
            "arm_length": lamp.armLength as Any,
            "inclination": lamp.inclination as Any,
            "lclass": lamp.lclass ?? "",
            "model": lamp.model ?? "",
            "type": lamp.type ?? "",
            "lfr": lamp.lfr as Any,
            "power": lamp.power as Any,
            "comment": lamp.comment ?? "",
            "location": [
                "coordinates": [
                    lamp.location.coordinate.longitude,
                    lamp.location.coordinate.latitude,
                ],
                "type": "Point",
            ],
        ]
        
        return parameters
    }
}
