//
//  LocationSerializer.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 20/08/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation

class LocationSerializer {
    
    static func toParams(location: CLLocation, radius: Float) -> Parameters {
        return [
            "latitude": location.coordinate.latitude,
            "longitude": location.coordinate.longitude,
            "radius": radius
        ]
    }
}
