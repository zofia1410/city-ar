//
//  Utils.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 18/08/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Foundation

func getDateFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    return dateFormatter
}
