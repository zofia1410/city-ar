//
//  StreetSerializer.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 18/08/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation

class StreetSerializer {
    private static let dateFormatter: DateFormatter = getDateFormatter()
    
    static func toStreet(json: JSON) -> Street {
        let id = json["_id"].stringValue
        let label = json["label"].stringValue
        var vertices: [CLLocationCoordinate2D] = []
        
        for (_, vertex) in json["location"]["coordinates"][0] {
            vertices.append(CLLocationCoordinate2D(latitude: Double(vertex[1].stringValue)!, longitude: Double(vertex[0].stringValue)!))
        }
        
        let center = CLLocation(coordinate: CLLocationCoordinate2D(latitude: Double(json["center"]["coordinates"][1].stringValue)!, longitude: Double(json["center"]["coordinates"][0].stringValue)!), altitude: Double(json["center"]["coordinates"][2].stringValue)!)
        
        let createdAt = dateFormatter.date(from: json["createdAt"].stringValue)!
        let updatedAt = dateFormatter.date(from: json["updatedAt"].stringValue)!
        
        let newStreet = Street(id: id, label: label, vertices: vertices, center: center, createdAt: createdAt, updatedAt: updatedAt)
        newStreet.street = json["street"].stringValue
        newStreet.type = json["type"].stringValue
        newStreet.comment = json["comment"].stringValue
        
        return newStreet
    }
}
