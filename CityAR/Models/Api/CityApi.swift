//
//  CityApi.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 18/08/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Alamofire
import CoreLocation
import Foundation
import SwiftyJSON

class CityApi: Api {
    init() {
        super.init(baseUrl: "https://city-ar.herokuapp.com/")
    }
    
    func getLamps(location: CLLocation, radius: Float, onSuccess: @escaping ([Lamp]) -> Void, onError: @escaping (Error) -> Void) {
        let parameters = LocationSerializer.toParams(location: location, radius: radius)
        
        Alamofire.request(baseUrl + "lamp/location", method: .get, parameters: parameters).responseJSON { response in
            if response.result.isSuccess {
                let lampsJSON: JSON = JSON(response.result.value!)
                var downloadedLamps: [Lamp] = []
                
                print("Got new lamps! - \(lampsJSON.count)")
                
                if lampsJSON.count != 0 {
                    for (_, lamp): (String, JSON) in lampsJSON {
                        downloadedLamps.append(LampSerializer.toLamp(json: lamp))
                    }
                }
                
                onSuccess(downloadedLamps)
            } else {
                onError(response.result.error!)
            }
        }
    }
    
    func addLamp(lamp: Lamp, onSuccess: @escaping (String) -> Void, onError: @escaping (Error) -> Void) {
        let parameters = LampSerializer.toParams(lamp: lamp)
        
        Alamofire.request(baseUrl + "lamp", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.isSuccess {
                let updatedLamp: JSON = JSON(response.result.value!)
                onSuccess(updatedLamp["_id"].stringValue)
            } else {
                onError(response.result.error!)
            }
        }
    }
    
    func updateLamp(lamp: Lamp, onSuccess: @escaping (Lamp) -> Void, onError: @escaping (Error) -> Void) {
        let parameters = LampSerializer.toParams(lamp: lamp)
        
        Alamofire.request(baseUrl + "lamp/" + lamp.id, method: .put, parameters: parameters,
                          encoding: JSONEncoding.default).responseJSON { response in
            if response.result.isSuccess {
                let updatedLamp: JSON = JSON(response.result.value!)
                onSuccess(LampSerializer.toLamp(json: updatedLamp))
            } else {
                onError(response.result.error!)
            }
        }
    }
    
    func deleteLamp(id: String, onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        Alamofire.request(baseUrl + "lamp/ " + id, method: .delete)
            .validate(statusCode: 200 ..< 300)
            .response { response in
                if let error = response.error {
                    onError(error)
                } else {
                    onSuccess()
                }
            }
    }
    
    func getStreets(location: CLLocation, radius: Float, onSuccess: @escaping ([Street]) -> Void, onError: @escaping (Error) -> Void) {
        let parameters = LocationSerializer.toParams(location: location, radius: radius)
        
        Alamofire.request(baseUrl + "street/location", method: .get, parameters: parameters).responseJSON { response in
            if response.result.isSuccess {
                let streetsJSON: JSON = JSON(response.result.value!)
                var downloadedStreets: [Street] = []
                
                print("Got new streets! - \(streetsJSON.count)")
                
                if streetsJSON.count != 0 {
                    for (_, street): (String, JSON) in streetsJSON {
                        downloadedStreets.append(StreetSerializer.toStreet(json: street))
                    }
                }
                onSuccess(downloadedStreets)
            } else {
                onError(response.result.error!)
            }
        }
    }
}
