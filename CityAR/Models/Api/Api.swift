//
//  Api.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 18/08/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Foundation

open class Api {
    internal let baseUrl: String

    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
}
