//
//  Lamp.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 09/06/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import ARCL
import CoreLocation
import MapKit
import SceneKit
import UIKit

class Lamp {
    let id: String
    var label: String
    var street: String?
    var height: Double?
    var overhang: Double?
    var setback: Double?
    var armLength: Double?
    var inclination: Double?
    var lclass: String? // lighting class
    var model: String?
    var type: String?
    var lfr: Double? // low-fluence responses
    var power: Double?
    var comment: String?
    var location: CLLocation
    var createdAt: Date
    var updatedAt: Date
    var lampView: LocationNode?
    var annotationNode: LocationAnnotationNode?
    var mapAnnotiation: MKPointAnnotation?
    var heightIndicator: ObjectIndicator?

    init(id: String, label: String, location: CLLocation, createdAt: Date, updatedAt: Date) {
        self.id = id
        self.label = label
        self.location = location
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

extension Lamp: Equatable {
    static func == (lhs: Lamp, rhs: Lamp) -> Bool {
        return
            lhs.id == rhs.id
    }
}
