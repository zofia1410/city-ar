//
//  Street.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 28/07/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import ARCL
import Foundation
import MapKit

class Street {
    let id: String
    var label: String
    var street: String?
    var type: String?
    var comment: String?
    let vertices: [CLLocationCoordinate2D]
    let center: CLLocation
    let createdAt: Date
    var updatedAt: Date
    var streetView: [LocationNode]?
    var annotationNode: LocationAnnotationNode?
    var polygon: MKPolygon?

    init(id: String, label: String, vertices: [CLLocationCoordinate2D], center: CLLocation, createdAt: Date, updatedAt: Date) {
        self.id = id
        self.label = label
        self.vertices = vertices
        self.center = center
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

extension Street: Equatable {
    static func == (lhs: Street, rhs: Street) -> Bool {
        return
            lhs.id == rhs.id
    }
}
