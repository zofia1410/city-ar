//
//  AccelerometerController.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 14/06/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import CoreMotion
import Foundation

class AccelerometerService {
    var motion = CMMotionManager()
    var timer: Timer?
    var isHorizontal: Bool?

    let accelerometerUpdateInterval = 0.2

    func startAccelerometers(callback: @escaping (Bool) -> Void) {
        // Make sure the accelerometer hardware is available.
        if motion.isAccelerometerAvailable {
            motion.accelerometerUpdateInterval = accelerometerUpdateInterval
            motion.startAccelerometerUpdates()

            // Configure a timer to fetch the data.
            timer = Timer(fire: Date(), interval: accelerometerUpdateInterval,
                          repeats: true, block: { _ in
                              // Get the accelerometer data.
                              if let data = self.motion.accelerometerData {
                                  let y = data.acceleration.y
                                  var horizontal = false
                                  if y > -0.45, y < 0.45 {
                                      horizontal = true
                                  }

                                  if horizontal != self.isHorizontal {
                                      self.isHorizontal = horizontal
                                      callback(horizontal)
                                  }
                              }
            })

            // Add the timer to the current run loop.
            RunLoop.current.add(timer!, forMode: .default)
        }
    }

    func stopAccelerometers() {
        motion.stopAccelerometerUpdates()
        timer?.invalidate()
        timer = nil
    }
}
