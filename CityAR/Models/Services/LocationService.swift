//
//  LocationController.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 15/06/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import CoreLocation
import Foundation

protocol LocationServiceDelegate {
    func tracingLocation(currentLocation: CLLocation)
    func tracingLocationDidFailWithError(error: Error)
}

class LocationService: NSObject, CLLocationManagerDelegate {
    static let sharedInstance: LocationService = {
        let instance: LocationService = LocationService()
        return instance
    }()

    var locationManager: CLLocationManager?
    var lastLocation: CLLocation?
    var delegate: LocationServiceDelegate?

    override init() {
        super.init()

        self.locationManager = CLLocationManager()
        guard let locationManager = self.locationManager else {
            return
        }

        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }

        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.headingFilter = kCLHeadingFilterNone
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        locationManager.delegate = self
    }

    func startUpdatingLocation() {
        print("Starting Location Updates")
        locationManager?.startUpdatingLocation()

        if lastLocation != nil {
            delegate?.tracingLocation(currentLocation: lastLocation!)
        }
    }

    func stopUpdatingLocation() {
        print("Stop Location Updates")
        locationManager?.stopUpdatingLocation()
    }

    func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        if lastLocation == nil {
            updateLocation(currentLocation: location)
            return
        }
        
        if location.horizontalAccuracy < lastLocation!.horizontalAccuracy {
            updateLocation(currentLocation: location)
        } else if location.horizontalAccuracy == lastLocation!.horizontalAccuracy
            && location.timestamp > lastLocation!.timestamp {
            updateLocation(currentLocation: location)
        }
    }

    func locationManager(_: CLLocationManager, didFailWithError error: Error) {
        updateLocationDidFailWithError(error: error)
    }

    private func updateLocation(currentLocation: CLLocation) {
        lastLocation = currentLocation
        
        guard let delegate = self.delegate else {
            return
        }

        delegate.tracingLocation(currentLocation: currentLocation)
    }

    private func updateLocationDidFailWithError(error: Error) {
        guard let delegate = self.delegate else {
            return
        }

        delegate.tracingLocationDidFailWithError(error: error)
    }
}
