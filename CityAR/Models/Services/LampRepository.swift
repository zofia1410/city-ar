//
//  LampRepository.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 15/06/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Alamofire
import CoreLocation
import Foundation
import SwiftyJSON

class LampRepository {
    static let api = CityApi()

    static func getLampsFromLocation(location: CLLocation, radius: Float, callback: @escaping ([Lamp]) -> Void) {
        api.getLamps(location: location, radius: radius, onSuccess: { lamps in
            callback(lamps)
        }) { error in
            print("Error \(String(describing: error))")
        }
    }

    static func deleteLamp(id: String) {
        api.deleteLamp(id: id, onSuccess: {
            print("Lamp \(id) deleted!")
        }) { error in
            print("Error \(String(describing: error))")
        }
    }

    static func updateLamp(lamp: Lamp, callback: @escaping (Lamp) -> Void) {
        api.updateLamp(lamp: lamp, onSuccess: { lamp in
            callback(lamp)
        }) { error in
            print("Error \(String(describing: error))")
        }
    }

    static func addLamp(lamp: Lamp, callback: @escaping (String) -> Void) {
        api.addLamp(lamp: lamp, onSuccess: { id in
            callback(id)
        }) { error in
            print("Error \(String(describing: error))")
        }
    }
}
