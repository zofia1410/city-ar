//
//  AddressManager.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 21/06/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import CoreLocation
import Foundation

struct AddressDetails {
    let street: String
    let number: String
    let postalCode: String
    let city: String

    static func fromPlacemark(placemark: CLPlacemark) -> AddressDetails {
        let street = placemark.thoroughfare ?? ""
        let number = placemark.subThoroughfare ?? ""
        let postlCode = placemark.postalCode ?? ""
        let city = placemark.locality ?? ""
        return AddressDetails(street: street, number: number, postalCode: postlCode, city: city)
    }

    func getFirstAddressLine() -> String {
        if !street.isEmpty, !number.isEmpty {
            return street + " " + number
        } else if !street.isEmpty {
            return street
        } else if !number.isEmpty {
            return number
        } else {
            return "-"
        }
    }

    func getSecondAddressLine() -> String {
        if !postalCode.isEmpty, !city.isEmpty {
            return postalCode + " " + city
        } else if !postalCode.isEmpty {
            return postalCode
        } else if !city.isEmpty {
            return city
        } else {
            return "-"
        }
    }
}

class AddressService {
    func getAddressDetails(forLocation location: CLLocation, completionHandler: @escaping (AddressDetails?) -> Void) {
        let geocoder = CLGeocoder()

        geocoder.reverseGeocodeLocation(location, completionHandler: {
            placemarks, error in

            if error != nil {
                completionHandler(nil)
            } else if let placemarkArray = placemarks {
                if let placemark = placemarkArray.first {
                    completionHandler(AddressDetails.fromPlacemark(placemark: placemark))
                } else {
                    completionHandler(nil)
                }
            } else {
                completionHandler(nil)
            }
        })
    }
}
