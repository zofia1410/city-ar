//
//  StreetRepository.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 28/07/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Alamofire
import CoreLocation
import Foundation
import MapKit
import SwiftyJSON

class StreetRepository {
    static let api = CityApi()

    static func getStreetsFromLocation(location: CLLocation, radius: Float, callback: @escaping ([Street]) -> Void) {
        api.getStreets(location: location, radius: radius, onSuccess: { streets in
            callback(streets)
        }) { error in
            print("Error \(String(describing: error))")
        }
    }
}
