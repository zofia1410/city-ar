//
//  StreetDetailsViewController.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 30/07/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class StreetDetailsViewController: UIViewController, MKMapViewDelegate {
    var activeStreet: Street?

    let addressService = AddressService()

    @IBOutlet var labelValue: UILabel!
    @IBOutlet var streetValue: UILabel!
    @IBOutlet var typeValue: UILabel!
    @IBOutlet var addressFirstLine: UILabel!
    @IBOutlet var addressSecondLine: UILabel!
    @IBOutlet var centerValue: UILabel!
    @IBOutlet var elevationValue: UILabel!
    @IBOutlet var createdAtDate: UILabel!
    @IBOutlet var updatedAtDate: UILabel!
    @IBOutlet var commentValue: UILabel!

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Street details"
        mapView.delegate = self
        mapView.mapType = MKMapType.hybrid
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false

        setStreetDetails()
        addPolygonOnMap()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        scrollView.contentSize = CGSize(width: view.frame.size.width, height: 900)
    }

    func setStreetDetails() {
        labelValue.text = activeStreet?.label
        streetValue.text = activeStreet?.street
        typeValue.text = activeStreet?.type

        addressService.getAddressDetails(forLocation: activeStreet!.center, completionHandler: { addressDetails in
            self.addressFirstLine.text = addressDetails?.getFirstAddressLine()
            self.addressSecondLine.text = addressDetails?.getSecondAddressLine()
        })

        centerValue.text = activeStreet!.center.coordinate.latitude.description + "°N " + activeStreet!.center.coordinate.longitude.description + "°E"
        elevationValue.text = activeStreet!.center.altitude.description + " m a.s.l."

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy, HH:mm:ss"
        createdAtDate.text = dateFormatter.string(from: activeStreet!.createdAt)
        updatedAtDate.text = dateFormatter.string(from: activeStreet!.updatedAt)

        commentValue.text = activeStreet!.comment
    }

    func mapView(_: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolygon {
            let renderer = MKPolygonRenderer(overlay: overlay)
            renderer.fillColor = UIColor.primaryColor!.withAlphaComponent(0.4)
            renderer.strokeColor = UIColor.primaryColor
            renderer.lineWidth = 2
            return renderer
        }
        return MKOverlayRenderer()
    }

    func addPolygonOnMap() {
        let polygon = MKPolygon(coordinates: activeStreet!.vertices, count: activeStreet!.vertices.count)
        mapView.addOverlay(polygon)
        centerMapOnLocation(activeStreet!.center, mapView: mapView)
    }

    func centerMapOnLocation(_ location: CLLocation, mapView: MKMapView) {
        let regionRadius: CLLocationDistance = 100
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}
