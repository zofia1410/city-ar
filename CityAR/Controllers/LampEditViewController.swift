//
//  ModalViewController.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 24/06/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Foundation
import MapKit
import UIKit

extension UIViewController {
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard)
        )

        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

class LampEditViewController: UIViewController, UITextFieldDelegate, MKMapViewDelegate {
    var isNewLamp: Bool = false

    var activeLamp: Lamp?

    var changedCoordinates: CLLocationCoordinate2D?

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var scrollView: UIScrollView!

    @IBOutlet var labelTextField: UITextField!
    @IBOutlet var streetTextField: UITextField!
    @IBOutlet var heightTextField: UITextField!
    @IBOutlet var overhangTextField: UITextField!
    @IBOutlet var setbackTextField: UITextField!
    @IBOutlet var armLengthTextField: UITextField!
    @IBOutlet var inclinationTextField: UITextField!
    @IBOutlet var modelTextField: UITextField!
    @IBOutlet var typeTextField: UITextField!
    @IBOutlet var lightingClassTextField: UITextField!
    @IBOutlet var lfrTextField: UITextField!
    @IBOutlet var powerTextField: UITextField!
    @IBOutlet var commentTextField: UITextField!

    @IBOutlet var removeButton: UIView!

    @IBAction func remove(_: Any) {
        let dialogMessage = UIAlertController(title: "Remove lamp?", message: "Are you sure you want to remove \(activeLamp?.label ?? "this") lamp?", preferredStyle: .alert)
        let delete = UIAlertAction(title: "Remove", style: .destructive, handler: { (_) -> Void in
            LampRepository.deleteLamp(id: self.activeLamp!.id)
            self.performSegue(withIdentifier: "backToViewController", sender: self)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (_) -> Void in
            print("Cancel button click...")
        }
        dialogMessage.addAction(delete)
        dialogMessage.addAction(cancel)
        present(dialogMessage, animated: true, completion: nil)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderWidth = 1.0
        textField.layer.borderColor = UIColor.blue.cgColor
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.lightGray.cgColor
    }

    @IBAction func cancel(_: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func save(_: Any) {
        activeLamp!.label = labelTextField.text!
        activeLamp!.street = streetTextField.text!
        activeLamp!.height = Double(heightTextField.text!)
        activeLamp!.overhang = Double(overhangTextField.text!)
        activeLamp!.setback = Double(setbackTextField.text!)
        activeLamp!.armLength = Double(armLengthTextField.text!)
        activeLamp!.inclination = Double(inclinationTextField.text!)
        activeLamp!.model = modelTextField.text!
        activeLamp!.type = typeTextField.text!
        activeLamp!.lclass = lightingClassTextField.text!
        activeLamp!.lfr = Double(lfrTextField.text!)
        activeLamp!.power = Double(powerTextField.text!)
        activeLamp!.comment = commentTextField.text!
        if changedCoordinates != nil {
            activeLamp!.location = CLLocation(latitude: changedCoordinates!.latitude, longitude: changedCoordinates!.longitude)
        }

        if isNewLamp {
            LampRepository.addLamp(lamp: activeLamp!) { id in
                print("Lamp \(id) added")
            }
        } else {
            LampRepository.updateLamp(lamp: activeLamp!) { lamp in
                print("Lamp \(lamp.id) updated")
            }
        }

        dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboard()
        mapView.mapType = MKMapType.hybrid
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: view.frame.size.width, height: 1700)
    }

    override func viewWillAppear(_: Bool) {
        labelTextField.text = activeLamp?.label
        labelTextField.delegate = self
        streetTextField.text = activeLamp?.street
        streetTextField.delegate = self
        heightTextField.text = activeLamp?.height?.description
        heightTextField.delegate = self
        overhangTextField.text = activeLamp?.overhang?.description
        overhangTextField.delegate = self
        setbackTextField.text = activeLamp?.setback?.description
        setbackTextField.delegate = self
        armLengthTextField.text = activeLamp?.armLength?.description
        armLengthTextField.delegate = self
        inclinationTextField.text = activeLamp?.inclination?.description
        inclinationTextField.delegate = self
        modelTextField.text = activeLamp?.model
        modelTextField.delegate = self
        typeTextField.text = activeLamp?.type
        typeTextField.delegate = self
        lightingClassTextField.text = activeLamp?.lclass
        lightingClassTextField.delegate = self
        lfrTextField.text = activeLamp?.lfr?.description
        lfrTextField.delegate = self
        powerTextField.text = activeLamp?.power?.description
        powerTextField.delegate = self
        commentTextField.text = activeLamp?.comment
        commentTextField.delegate = self
        mapView.delegate = self
        addPinOnMap()

        if isNewLamp {
            removeButton.isHidden = true
        }
    }

    // Edit pin location

    func addPinOnMap() {
        let annotation = MKPointAnnotation()
        annotation.coordinate = activeLamp!.location.coordinate
        mapView.addAnnotation(annotation)
        centerMapOnLocation(activeLamp!.location, mapView: mapView)
    }

    func centerMapOnLocation(_ location: CLLocation, mapView: MKMapView) {
        let regionRadius: CLLocationDistance = 200
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    func mapView(_: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKPointAnnotation {
            let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myPin")
            pinAnnotationView.pinTintColor = .red
            pinAnnotationView.isDraggable = true
            pinAnnotationView.canShowCallout = true
            pinAnnotationView.animatesDrop = true

            return pinAnnotationView
        }
        return nil
    }

    func mapView(_: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationView.DragState, fromOldState _: MKAnnotationView.DragState) {
        switch newState {
        case .starting:

            view.dragState = .dragging
        case .ending, .canceling:
            changedCoordinates = view.annotation?.coordinate
            view.dragState = .none
        default: break
        }
    }
}
