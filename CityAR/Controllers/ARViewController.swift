//
//  ViewController.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 25/05/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import ARCL
import ARKit
import CoreLocation
import MapKit
import UIKit

class ARViewController: UIViewController, LocationServiceDelegate, ARSCNViewDelegate {
    var arViewManager: ARViewManager?
    let accelerometer = AccelerometerService()
    let locationService = LocationService.sharedInstance
    let interval: Float = 5
    var currentRadius: Float = 10
    var isScreenLocked: Bool = false
    var areLampsVisible: Bool = true
    var areStreetsVisible: Bool = true
    var lastSliderUpdate = Date()
    
    @IBOutlet var sceneLocationView: SceneLocationView!
    @IBOutlet var descriptionView: UIView!
    @IBOutlet var userAltitude: UILabel!
    @IBOutlet var numberOfVisibleLamps: UILabel!
    @IBOutlet var numberOfVisibleStreets: UILabel!
    @IBOutlet var nameValue: UILabel!
    @IBOutlet var elevationValue: UILabel!
    @IBOutlet var heightValue: UILabel!
    @IBOutlet var addressFirstLine: UILabel!
    @IBOutlet var addressSecondLine: UILabel!
    @IBOutlet var lockImage: UIImageView!
    @IBOutlet var rangeSlider: UISlider!
    @IBOutlet var rangeValue: UILabel!
    @IBOutlet var descriptionIcon: UIImageView!
    @IBOutlet var lampIcon: UIImageView!
    @IBOutlet var streetIcon: UIImageView!
    
    @IBAction func hideLamps(_: Any) {
        let currentLocation = locationService.lastLocation
        
        if areLampsVisible {
            arViewManager?.clearSelection()
            lampIcon.image = UIImage(named: "lamp_off")
            lampIcon.tintColor = UIColor.lightGray
            
            arViewManager?.removeAllLamps()
            numberOfVisibleLamps.text = arViewManager?.getLampCount().description
        }
        else {
            lampIcon.image = UIImage(named: "lamp")
            lampIcon.tintColor = UIColor.primaryColor
            if currentLocation != nil {
                getNewLamps(currentLocation: currentLocation!)
            }
        }
        
        areLampsVisible = !areLampsVisible
    }
    
    @IBAction func hideStreets(_: Any) {
        let currentLocation = locationService.lastLocation
        
        if areStreetsVisible {
            arViewManager?.clearSelection()
            streetIcon.image = UIImage(named: "street_off")
            streetIcon.tintColor = UIColor.lightGray
            
            arViewManager?.removeAllStreets()
            numberOfVisibleStreets.text = arViewManager?.getStreetCount().description
        }
        else {
            streetIcon.image = UIImage(named: "street")
            streetIcon.tintColor = UIColor.primaryColor
            if currentLocation != nil {
                getNewStreets(currentLocation: currentLocation!)
            }
        }
        
        areStreetsVisible = !areStreetsVisible
    }
    
    func updateSliderValue() {
        arViewManager?.clearSelection()
        let roundedValue = round(rangeSlider.value / interval) * interval
        rangeSlider.value = roundedValue
        currentRadius = roundedValue
        rangeValue.text = Int(roundedValue).description + " m"
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .moved:
                updateSliderValue()
                let currentLocation = locationService.lastLocation
                if currentLocation != nil && abs(lastSliderUpdate.timeIntervalSinceNow) > 0.25 {
                    getNewItems(currentLocation: locationService.lastLocation!)
                    lastSliderUpdate = Date()
                }
            case .ended:
                updateSliderValue()
                let currentLocation = locationService.lastLocation
                if currentLocation != nil {
                    getNewItems(currentLocation: locationService.lastLocation!)
                }
            default:
                break
            }
        }
        
    }
    
    
    @IBAction func unwindToViewController(segue _: UIStoryboardSegue) {}
    
    @IBAction func openMoreInformation(_: Any) {
        if ((arViewManager?.activeLamp) != nil) {
            performSegue(withIdentifier: "openLampInformation", sender: self)
        }
        else if ((arViewManager?.activeStreet) != nil) {
            performSegue(withIdentifier: "openStreetInformation", sender: self)
        }
    }
    
    @IBAction func closeDescriptionView(_: Any) {
        arViewManager?.clearSelection()
    }
    
    @IBAction func addNewLampButton(_: Any) {
        performSegue(withIdentifier: "openNewLamp", sender: self)
    }
    
    @IBAction func lockAR(_: Any) {
        if isScreenLocked == false {
            lockImage.image = UIImage(named: "lock_close")
            isScreenLocked = true
        }
        else if isScreenLocked == true {
            lockImage.image = UIImage(named: "lock_open")
            isScreenLocked = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let descriptionViewHolder = DescriptionViewHolder(description: descriptionView, descriptionIcon: descriptionIcon, name: nameValue, elevation: elevationValue, height: heightValue, addressFirstLine: addressFirstLine, addressSecondLine: addressSecondLine)
        arViewManager = ARViewManager(sceneLocationView: sceneLocationView, descriptionVH: descriptionViewHolder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
        rangeValue.text = Int(currentRadius).description + " m"
        numberOfVisibleLamps.text = arViewManager?.getLampCount().description
        numberOfVisibleStreets.text = arViewManager?.getStreetCount().description
        descriptionIcon.tintColor = UIColor.white
        rangeSlider.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)

        sceneLocationView.session.configuration?.isLightEstimationEnabled = true
        sceneLocationView.run()
        
        locationService.delegate = self
        locationService.startUpdatingLocation()
        
        accelerometer.startAccelerometers(callback: { isHorizontal in
            if isHorizontal, self.isScreenLocked == false {
                self.performSegue(withIdentifier: "moveToMap", sender: nil)
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        super.viewWillDisappear(animated)
        arViewManager?.clearSelection()
        locationService.stopUpdatingLocation()
        accelerometer.stopAccelerometers()
        sceneLocationView.pause()
    }
    
    func tracingLocation(currentLocation: CLLocation) {
        userAltitude.text = "User elevation: " + NSString(format: "%.1f", currentLocation.altitude).description + " m a.s.l."
        getNewItems(currentLocation: currentLocation)
    }
    
    func tracingLocationDidFailWithError(error _: Error) {
        print("Location error")
    }
    
    // Get lamps and streets
    func getNewItems(currentLocation: CLLocation) {
        if areLampsVisible {
            getNewLamps(currentLocation: currentLocation)
        }
        if areStreetsVisible {
            getNewStreets(currentLocation: currentLocation)
        }
    }
    
    func getNewLamps(currentLocation: CLLocation) {
        LampRepository.getLampsFromLocation(location: currentLocation, radius: currentRadius / 1000, callback: { lamps in
            self.arViewManager?.updateLamps(newLamps: lamps)
            self.numberOfVisibleLamps.text = self.arViewManager?.getLampCount().description
        })
    }
    
    func getNewStreets(currentLocation: CLLocation) {
        StreetRepository.getStreetsFromLocation(location: currentLocation, radius: currentRadius / 1000, callback: { streets in
            self.arViewManager?.updateStreets(newStreets: streets)
            self.numberOfVisibleStreets.text = self.arViewManager?.getStreetCount().description
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with _: UIEvent?) {
        if let touchLocation = touches.first?.location(in: sceneLocationView) {
            if let hit = sceneLocationView.hitTest(touchLocation, options: nil).first {
                self.arViewManager?.handleOnClick(hit: hit)
            }
            return
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "openLampInformation" {
            let lampDetailsVC = segue.destination as! LampDetailsViewController
            lampDetailsVC.activeLamp = arViewManager!.activeLamp
        }
        else if segue.identifier == "openStreetInformation" {
            let streetDetailsVC = segue.destination as! StreetDetailsViewController
            streetDetailsVC.activeStreet = arViewManager!.activeStreet
        }
    }
}
