//
//  DetailsViewController.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 21/06/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import CoreLocation
import Foundation
import MapKit
import UIKit

class LampDetailsViewController: UIViewController {
    var activeLamp: Lamp?

    let addressService = AddressService()

    @IBOutlet var labelValue: UILabel!
    @IBOutlet var streetValue: UILabel!
    @IBOutlet var heightValue: UILabel!
    @IBOutlet var overhangValue: UILabel!
    @IBOutlet var setbackValue: UILabel!
    @IBOutlet var armLengthValue: UILabel!
    @IBOutlet var inclinationValue: UILabel!
    @IBOutlet var modelValue: UILabel!
    @IBOutlet var typeValue: UILabel!
    @IBOutlet var lightingClassValue: UILabel!
    @IBOutlet var lowFluenceResponsesValue: UILabel!
    @IBOutlet var powerValue: UILabel!
    @IBOutlet var addressFirstLine: UILabel!
    @IBOutlet var addressSecondLine: UILabel!
    @IBOutlet var coordinatesValue: UILabel!
    @IBOutlet var elevationValue: UILabel!
    @IBOutlet var updatedAtDate: UILabel!
    @IBOutlet var createdAtDate: UILabel!
    @IBOutlet var comment: UILabel!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Lamp details"
    }

    override func viewWillAppear(_ animated: Bool) {
        setLampDetails()
        addPinOnMap()

        navigationController?.navigationBar.isHidden = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.edit, target: self, action: #selector(openModal))
        super.viewWillAppear(animated)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        scrollView.contentSize = CGSize(width: view.frame.size.width, height: 1250)
    }

    @objc func openModal() {
        performSegue(withIdentifier: "openModal", sender: self)
    }

    func setLampDetails() {
        labelValue.text = activeLamp?.label
        streetValue.text = activeLamp?.street

        heightValue.text = activeLamp?.height?.description ?? "-"
        overhangValue.text = activeLamp?.overhang?.description ?? "-"
        setbackValue.text = activeLamp?.setback?.description ?? "-"
        armLengthValue.text = activeLamp?.armLength?.description ?? "-"
        inclinationValue.text = activeLamp?.inclination?.description ?? "-"

        modelValue.text = (activeLamp?.model?.isEmpty)! ? "-" : activeLamp?.model!
        typeValue.text = (activeLamp?.type?.isEmpty)! ? "-" : activeLamp?.type!

        lightingClassValue.text = (activeLamp?.lclass?.isEmpty)! ? "-" : activeLamp?.lclass!
        lowFluenceResponsesValue.text = activeLamp?.lfr?.description ?? "-"
        powerValue.text = activeLamp?.power?.description ?? "-"

        addressService.getAddressDetails(forLocation: activeLamp!.location, completionHandler: { addressDetails in
            self.addressFirstLine.text = addressDetails?.getFirstAddressLine()
            self.addressSecondLine.text = addressDetails?.getSecondAddressLine()
        })

        coordinatesValue.text = activeLamp!.location.coordinate.latitude.description + "°N " + activeLamp!.location.coordinate.longitude.description + "°E"
        elevationValue.text = activeLamp!.location.altitude.description + " m a.s.l."

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy, HH:mm:ss"
        createdAtDate.text = dateFormatter.string(from: activeLamp!.createdAt)
        updatedAtDate.text = dateFormatter.string(from: activeLamp!.updatedAt)

        comment.text = (activeLamp?.comment?.isEmpty)! ? "-" : activeLamp?.comment!
    }

    func addPinOnMap() {
        mapView.removeAnnotations(mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = activeLamp!.location.coordinate
        mapView.addAnnotation(annotation)
        centerMapOnLocation(activeLamp!.location, mapView: mapView)
    }

    func centerMapOnLocation(_ location: CLLocation, mapView: MKMapView) {
        let regionRadius: CLLocationDistance = 400
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "openModal" {
            let modalVC = segue.destination as! LampEditViewController
            modalVC.activeLamp = activeLamp
        }
    }
}
