//
//  AddNewLampController.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 03/08/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import CoreLocation
import Foundation
import UIKit

class AddNewLampController: UIViewController, LocationServiceDelegate {
    let locationService = LocationService.sharedInstance

    @IBAction func closeModal(_: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func createNewLamp(_: Any) {
        performSegue(withIdentifier: "completeLampDetails", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func tracingLocation(currentLocation _: CLLocation) {
        print("Got location")
    }

    func tracingLocationDidFailWithError(error _: Error) {
        print("Location error")
    }

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "completeLampDetails" {
            let editVC = segue.destination as! LampEditViewController
            let currentLocation = locationService.lastLocation
            let newLamp = Lamp(id: "", label: "", location: currentLocation!, createdAt: Date(), updatedAt: Date())
            editVC.activeLamp = newLamp
            editVC.isNewLamp = true
        }
    }
}
