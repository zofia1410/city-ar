//
//  ARViewManager.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 20/08/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import ARCL
import ARKit
import CoreLocation
import Foundation
import UIKit

class ARViewManager {
    private var displayedLamps: [Lamp] = []
    private var displayedStreets: [Street] = []
    
    var activeLamp: Lamp?
    var activeStreet: Street?
    
    private let sceneLocationView: SceneLocationView
    private let descriptionViewHolder: DescriptionViewHolder
    
    private let addressService = AddressService()
    
    init(sceneLocationView: SceneLocationView, descriptionVH: DescriptionViewHolder) {
        self.sceneLocationView = sceneLocationView
        self.descriptionViewHolder = descriptionVH
    }
    
    // item counting
    func getLampCount() -> Int {
        return displayedLamps.count
    }
    
    func getStreetCount() -> Int {
        return displayedStreets.count
    }
    
    // update methods
    func updateLamps(newLamps: [Lamp]) {
        // add new lamps
        for lamp in newLamps {
            if !displayedLamps.contains(lamp) {
                addLamp(lamp: lamp, active: false)
            }
        }
        
        // remove lamps not in range
        for (i, lamp) in displayedLamps.enumerated().reversed() {
            if !newLamps.contains(lamp) {
                removeLamp(lamp: lamp, index: i)
            }
        }
    }
    
    func updateStreets(newStreets: [Street]) {
        // add new lamps
        for street in newStreets {
            if !displayedStreets.contains(street) {
                addStreet(street: street, active: false)
            }
        }
        
        // remove lamps not in range
        for (i, street) in displayedStreets.enumerated().reversed() {
            if !newStreets.contains(street) {
                removeStreet(street: street, index: i)
            }
        }
    }
    
    // add methods
    private func addLamp(lamp: Lamp, active: Bool) {
        let height = lamp.height ?? 0
        let bottom = lamp.location
        let altitude = lamp.location.altitude + height
        
        let top = CLLocation(coordinate: lamp.location.coordinate, altitude: altitude)
        
        if lamp.lampView != nil {
            sceneLocationView.removeLocationNode(locationNode: lamp.lampView!)
            lamp.lampView = nil
        }
        if lamp.annotationNode != nil {
            sceneLocationView.removeLocationNode(locationNode: lamp.annotationNode!)
            lamp.annotationNode = nil
        }
        
        if active {
            lamp.annotationNode = addIndicator(location: CLLocation(coordinate: lamp.location.coordinate, altitude: altitude), color: UIColor.selectedColor!, fontSize: 28, text: String(format: "%.2f m", lamp.height!))
            lamp.lampView = LampBuilder(top: top, bottom: bottom, color: UIColor.selectedColor!).lampNode
        }
        else {
            lamp.annotationNode = addIndicator(location: CLLocation(coordinate: lamp.location.coordinate, altitude: altitude), color: UIColor.primaryColor!, fontSize: 28, text: String(format: "%.2f m", lamp.height!))
            lamp.lampView = LampBuilder(top: top, bottom: bottom, color: UIColor.primaryColor!).lampNode
        }
        sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: lamp.annotationNode!)
        sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: lamp.lampView!)
        displayedLamps.append(lamp)
    }
    
    private func addStreet(street: Street, active: Bool) {
        let polyline = street.vertices
        let altitude = street.center.altitude - 2
        
        if street.streetView != nil {
            sceneLocationView.removeLocationNodes(locationNodes: street.streetView!)
            street.streetView = nil
        }
        if street.annotationNode != nil {
            sceneLocationView.removeLocationNode(locationNode: street.annotationNode!)
            street.annotationNode = nil
        }
        
        if active {
            street.annotationNode = addIndicator(location: CLLocation(coordinate: street.center.coordinate, altitude: altitude+1), color: UIColor.selectedColor!, fontSize: 64, text: street.vertices.count.description)
            street.streetView = StreetBuilder(polyline: polyline, altitude: altitude, color: UIColor.selectedColor!).streetNodes
        }
        else {
            street.annotationNode = addIndicator(location: CLLocation(coordinate: street.center.coordinate, altitude: altitude+1), color: UIColor.primaryColor!, fontSize: 64, text: street.vertices.count.description)
            street.streetView = StreetBuilder(polyline: polyline, altitude: altitude, color: UIColor.primaryColor!).streetNodes
        }
        sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: street.annotationNode!)
        sceneLocationView.addLocationNodesWithConfirmedLocation(locationNodes: street.streetView!)
        displayedStreets.append(street)
    }
    
    private func addIndicator(location: CLLocation, color: UIColor, fontSize: CGFloat, text: String) -> LocationAnnotationNode {
        let indicator = ObjectIndicator(frame: CGRect(x: 0, y: 0, width: 150, height: 150))
        indicator.value.font = indicator.value.font.withSize(fontSize)
        indicator.value.text = text
        indicator.view.backgroundColor = color
        
        let annotationNode = LocationAnnotationNode(location: location, view: indicator)
        annotationNode.scaleRelativeToDistance = true
        sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: annotationNode)
        return annotationNode
    }
    
    // remove methods
    func removeLamp(lamp: Lamp) {
        let index = displayedLamps.lastIndex(of: lamp)
        if  index != nil {
            removeLamp(lamp: lamp, index: index!)
        } else {
            print("Cannot remove not displayed lamp")
        }
    }
    
    func removeStreet(street: Street) {
        let index = displayedStreets.lastIndex(of: street)
        if  index != nil {
            removeStreet(street: street, index: index!)
        } else {
            print("Cannot remove not displayed street")
        }
    }
    
    private func removeLamp(lamp: Lamp, index: Int) {
        if lamp.lampView != nil {
            sceneLocationView.removeLocationNode(locationNode: lamp.lampView!)
        }
        if lamp.annotationNode != nil {
            sceneLocationView.removeLocationNode(locationNode: lamp.annotationNode!)
        }
        
        lamp.lampView = nil
        lamp.annotationNode = nil
    
        displayedLamps.remove(at: index)
    }
    
    private func removeStreet(street: Street, index: Int) {
        if street.streetView != nil {
            sceneLocationView.removeLocationNodes(locationNodes: street.streetView!)
        }
        if street.annotationNode != nil {
            sceneLocationView.removeLocationNode(locationNode: street.annotationNode!)
        }
        
        street.streetView = nil
        street.annotationNode = nil
        
        displayedStreets.remove(at: index)
    }
    
    func removeAllLamps() {
        for (i, lamp) in displayedLamps.enumerated().reversed() {
            removeLamp(lamp: lamp, index: i)
        }
    }
    
    func removeAllStreets() {
        for (i, street) in displayedStreets.enumerated().reversed() {
            removeStreet(street: street, index: i)
        }
    }
    
    private func setActiveState(lamp: Lamp, isActive: Bool) {
        removeLamp(lamp: lamp)
        addLamp(lamp: lamp, active: isActive)
    }
    
    private func setActiveState(street: Street, isActive: Bool) {
        removeStreet(street: street)
        addStreet(street: street, active: isActive)
    }
    
    func clearSelection() {
        descriptionViewHolder.description.isHidden = true
        if activeStreet != nil {
            setActiveState(street: activeStreet!, isActive: false)
        }
        activeStreet = nil
        
        if activeLamp != nil {
            setActiveState(lamp: activeLamp!, isActive: false)
        }
        activeLamp = nil
    }
    
    // description
    private func displayDescription(lamp: Lamp) {
        displayDescription(imageName: "lamp", name: lamp.label, altitude: lamp.location.altitude, additionalText: "Height: " +  String(format: "%.1f", lamp.height!) + " m", location: lamp.location)
    }
    
    private func displayDescription(street: Street) {
        displayDescription(imageName: "street", name: street.label, altitude: street.center.altitude, additionalText: "Segments: " + street.vertices.count.description, location: street.center)
    }
    
    private func displayDescription(imageName: String, name: String, altitude: CLLocationDistance, additionalText: String, location: CLLocation) {
        descriptionViewHolder.description.isHidden = false
        descriptionViewHolder.descriptionIcon.image = UIImage(named: imageName)
        descriptionViewHolder.name.text = name
        descriptionViewHolder.elevation.text = "Elevation: " + String(format: "%.1f", altitude) + " m a.s.l."
        descriptionViewHolder.additionalText.text = additionalText
        addressService.getAddressDetails(forLocation: location, completionHandler: { addressDetails in
            self.descriptionViewHolder.addressFirstLine.text = addressDetails?.getFirstAddressLine()
            self.descriptionViewHolder.addressSecondLine.text = addressDetails?.getSecondAddressLine()
        })
    }
    
    // onClick handler
    func handleOnClick(hit: SCNHitTestResult) {
        for lamp in displayedLamps {
            if lamp.lampView?.childNodes[0] == hit.node || lamp.annotationNode?.childNodes[0] == hit.node {
                if lamp != activeLamp {
                    clearSelection()
                    setActiveState(lamp: lamp, isActive: true)
                    activeLamp = lamp
                    displayDescription(lamp: lamp)
                } else {
                    clearSelection()
                }
                break
            }
        }
        
        for (i, street) in displayedStreets.enumerated() {
            if street.streetView?[i].childNodes[0] == hit.node || street.annotationNode?.childNodes[0] == hit.node {
                if street != activeStreet {
                    clearSelection()
                    setActiveState(street: street, isActive: true)
                    activeStreet = street
                    displayDescription(street: street)
                } else {
                    clearSelection()
                }
                break
            }
        }
    }
}

class DescriptionViewHolder {
    let description: UIView
    let descriptionIcon: UIImageView
    let name: UILabel
    let elevation: UILabel
    let additionalText: UILabel
    let addressFirstLine: UILabel
    let addressSecondLine: UILabel
    
    init(description: UIView, descriptionIcon: UIImageView, name: UILabel, elevation: UILabel, height: UILabel, addressFirstLine: UILabel, addressSecondLine: UILabel) {
        self.description = description
        self.descriptionIcon = descriptionIcon
        self.name = name
        self.elevation = elevation
        self.additionalText = height
        self.addressFirstLine = addressFirstLine
        self.addressSecondLine = addressSecondLine
    }
}
