//
//  MapViewController.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 14/06/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class MapViewController: UIViewController, MKMapViewDelegate, LocationServiceDelegate {
    let accelerometer = AccelerometerService()
    let locationController = LocationService.sharedInstance
    let span = MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002)

    var lampsOnMap: [Lamp] = []
    var streetsOnMap: [Street] = []
    var activeLamp: Lamp?
    var activeStreet: Street?
    var shouldCenter: Bool = true
    var isScreenLocked: Bool = false

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var lockImage: UIImageView!

    @IBAction func lockMap(_: Any) {
        if isScreenLocked == false {
            lockImage.image = UIImage(named: "lock_close")
            isScreenLocked = true
        } else if isScreenLocked == true {
            lockImage.image = UIImage(named: "lock_open")
            isScreenLocked = false
        }
    }

    @IBAction func changeMapType(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            mapView.mapType = MKMapType.standard
        } else if sender.selectedSegmentIndex == 1 {
            mapView.mapType = MKMapType.hybrid
        } else if sender.selectedSegmentIndex == 2 {
            mapView.mapType = MKMapType.satellite
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        locationController.delegate = self

        mapView.register(CustomAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapOnPolygon(_:)))
        mapView.addGestureRecognizer(tap)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        navigationController?.navigationBar.isHidden = true

        locationController.startUpdatingLocation()

        clearMap()
        getNewItems()

        accelerometer.startAccelerometers(callback: { isHorizontal in
            if !isHorizontal, self.isScreenLocked == false {
                self.navigationController?.popToRootViewController(animated: false)
            }
        })
    }

    override func viewWillDisappear(_: Bool) {
        locationController.stopUpdatingLocation()
        accelerometer.stopAccelerometers()
    }

    func tracingLocation(currentLocation: CLLocation) {
        if shouldCenter {
            let viewRegion = MKCoordinateRegion(center: currentLocation.coordinate, span: span)
            mapView.setRegion(viewRegion, animated: true)
            shouldCenter = false
        }
    }

    func tracingLocationDidFailWithError(error _: Error) {
        print("Location error")
    }

    func mapView(_: MKMapView, regionDidChangeAnimated _: Bool) {
        getNewItems()
    }

    func mapView(_: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped _: UIControl) {
        for lamp in lampsOnMap {
            if lamp.mapAnnotiation == view.annotation as? MKPointAnnotation {
                activeLamp = lamp
                performSegue(withIdentifier: "openLampDetails", sender: view)
            }
        }
    }

    class CustomAnnotationView: MKMarkerAnnotationView {
        override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
            super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)

            canShowCallout = true
            rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            rightCalloutAccessoryView?.tintColor = UIColor.primaryColor
            markerTintColor = UIColor.primaryColor
        }

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        }
    }

    func mapView(_: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolygon {
            let renderer = MKPolygonRenderer(overlay: overlay)
            renderer.fillColor = UIColor.primaryColor!.withAlphaComponent(0.4)
            renderer.strokeColor = UIColor.primaryColor
            renderer.lineWidth = 2
            return renderer
        }
        return MKOverlayRenderer()
    }

    @objc func handleTapOnPolygon(_ gesture: UITapGestureRecognizer) {
        let point = gesture.location(in: mapView)
        let coordinate = mapView.convert(point, toCoordinateFrom: nil)
        let mapPoint = MKMapPoint(coordinate)
        for overlay in mapView.overlays {
            if let polygon = overlay as? MKPolygon {
                guard let renderer = self.mapView.renderer(for: polygon) as? MKPolygonRenderer else { continue }
                let tapPoint = renderer.point(for: mapPoint)
                if renderer.path.contains(tapPoint) {
                    for street in streetsOnMap {
                        if polygon == street.polygon {
                            activeStreet = street
                            performSegue(withIdentifier: "openStreetDetails", sender: self)
                        }
                    }
                    break
                }
                continue
            }
        }
    }

    func getNewItems() {
        let centralLocation = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
        let radius = getRadius(centralLocation: centralLocation)

        LampRepository.getLampsFromLocation(location: CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude), radius: radius, callback: { lamps in
            for lamp in lamps {
                if !self.lampsOnMap.contains(lamp) {
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = lamp.location.coordinate
                    annotation.title = lamp.label
                    annotation.subtitle = lamp.model
                    self.mapView.addAnnotation(annotation)
                    lamp.mapAnnotiation = annotation
                    self.lampsOnMap.append(lamp)
                }
            }
        })

        StreetRepository.getStreetsFromLocation(location: CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude), radius: radius, callback: { streets in
            for street in streets {
                if !self.streetsOnMap.contains(street) {
                    street.polygon = MKPolygon(coordinates: street.vertices, count: street.vertices.count)
                    self.mapView.addOverlay(street.polygon!)
                    self.streetsOnMap.append(street)
                }
            }
        })
    }

    func clearMap() {
        mapView.removeAnnotations(mapView.annotations)
        lampsOnMap.removeAll()
        mapView.removeOverlays(mapView.overlays)
        streetsOnMap.removeAll()
    }

    func getRadius(centralLocation: CLLocation) -> Float {
        let topCentralLat: Double = centralLocation.coordinate.latitude - mapView.region.span.latitudeDelta / 2
        let topCentralLocation = CLLocation(latitude: topCentralLat, longitude: centralLocation.coordinate.longitude)
        let radius = centralLocation.distance(from: topCentralLocation)
        return Float(radius / 1000.0)
    }

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "openLampDetails" {
            let lampDetailsVC = segue.destination as! LampDetailsViewController
            lampDetailsVC.activeLamp = activeLamp
        } else if segue.identifier == "openStreetDetails" {
            let streetDetailsVC = segue.destination as! StreetDetailsViewController
            streetDetailsVC.activeStreet = activeStreet
        }
    }
}
