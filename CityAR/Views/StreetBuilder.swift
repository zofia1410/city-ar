//
//  StreetNodes.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 06/08/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import ARCL
import Foundation
import MapKit
import SceneKit

public class StreetBuilder {
    public private(set) var streetNodes = [LocationNode]()

    public let polyline: [CLLocationCoordinate2D]
    public let altitude: CLLocationDistance
    public let color: UIColor
    public let boxBuilder: BoxBuilder
    public init(polyline: [CLLocationCoordinate2D], altitude: CLLocationDistance, color: UIColor, boxBuilder: BoxBuilder? = nil) {
        self.polyline = polyline
        self.altitude = altitude
        self.color = color
        self.boxBuilder = boxBuilder ?? Constants.defaultBuilder

        contructNodes()
    }
}

private extension StreetBuilder {
    struct Constants {
        static let defaultBuilder: BoxBuilder = { (distance, color) -> SCNBox in
            let box = SCNBox(width: 0.5, height: 0.5, length: distance, chamferRadius: 0.25)
            box.firstMaterial?.diffuse.contents = color
            return box
        }
    }

    func contructNodes() {
        var points = polyline
        if points.count > 2 {
            points.append(polyline[0])
        }

        for i in 0 ..< points.count - 1 {
            let startPointLocation = CLLocation(coordinate: points[i], altitude: altitude)
            let endPointLocation = CLLocation(coordinate: points[i + 1], altitude: altitude)

            let distance = startPointLocation.distance(from: endPointLocation)

            let box = boxBuilder(CGFloat(distance), color)
            let boxNode = SCNNode(geometry: box)

            let bearing = -startPointLocation.bearing(between: endPointLocation)

            boxNode.pivot = SCNMatrix4MakeTranslation(0, 0, 0.5 * Float(distance))
            boxNode.eulerAngles.y = Float(bearing).degreesToRadians

            let streetNode = LocationNode(location: startPointLocation)
            streetNode.addChildNode(boxNode)

            streetNode.scaleRelativeToDistance = true

            streetNodes.append(streetNode)
        }
    }
}
