//
//  GradientView.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 07/07/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Foundation
import UIKit

class GradientView: UIView {
    open override class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [UIColor.black.withAlphaComponent(0.5).cgColor, UIColor.clear.cgColor]
    }
}
