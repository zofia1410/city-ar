//
//  TextFieldView.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 14/07/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import Foundation
import UIKit

class TextFieldView: UITextField, UITextFieldDelegate {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!

        layer.borderWidth = 1.0
        layer.cornerRadius = 4.0
        layer.borderColor = UIColor(red: 224.0 / 255.0, green: 224.0 / 255.0, blue: 224.0 / 255.0, alpha: 1.0).cgColor
    }
}
