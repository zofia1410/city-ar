//
//  Line.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 21/07/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import ARCL
import Foundation
import MapKit
import SceneKit

public typealias BoxBuilder = (_ distance: CGFloat, _ color: UIColor) -> SCNBox

public class LampBuilder {
    public private(set) var lampNode: LocationNode?

    public let top: CLLocation
    public let bottom: CLLocation
    public let boxBuilder: BoxBuilder
    public let color: UIColor
    public init(top: CLLocation, bottom: CLLocation, color: UIColor, boxBuilder: BoxBuilder? = nil) {
        self.top = top
        self.bottom = bottom
        self.color = color
        self.boxBuilder = boxBuilder ?? Constants.defaultBuilder

        contructNodes()
    }
}

private extension LampBuilder {
    struct Constants {
        static let defaultBuilder: BoxBuilder = { (height, color)  -> SCNBox in
            let box = SCNBox(width: 0.25, height: height + 1, length: 0.25, chamferRadius: 0.125)
            box.firstMaterial?.diffuse.contents = color
            return box
        }
    }

    func contructNodes() {
        let topOfLamp = top
        let bottomOfLamp = bottom

        let height = fabs(topOfLamp.altitude - bottomOfLamp.altitude)
        let box = boxBuilder(CGFloat(height), color)
        let boxNode = SCNNode(geometry: box)

        let locationNode = LocationNode(location: CLLocation(coordinate: bottomOfLamp.coordinate, altitude: bottomOfLamp.altitude + (height / 2)))
        locationNode.addChildNode(boxNode)

        locationNode.scaleRelativeToDistance = true
        
        lampNode = locationNode
    }
}
