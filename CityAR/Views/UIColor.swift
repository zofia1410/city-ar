//
//  UIColor.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 17/08/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import UIKit

extension UIColor {
    static let primaryColor = UIColor(named: "primaryColor")
    static let selectedColor = UIColor(named: "selectedColor")
}
