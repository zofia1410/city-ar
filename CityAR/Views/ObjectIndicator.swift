//
//  HeightIndicator.swift
//  CityAR
//
//  Created by Zofia Bartyzel on 28/07/2019.
//  Copyright © 2019 Zofia Bartyzel. All rights reserved.
//

import UIKit

extension UIView {
    func circle() {
        layer.cornerRadius = frame.width / 2
        layer.masksToBounds = true
    }
}

@IBDesignable
class ObjectIndicator: UIView {
    @IBOutlet var value: UILabel!
    @IBOutlet var view: UIView!

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }

    func loadViewFromNib() {
        Bundle.main.loadNibNamed("HeightIndicator", owner: self, options: nil)
        addSubview(view)
        view.frame = bounds
        view.circle()
    }
}
